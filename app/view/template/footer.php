<footer>
	<div id="footer">
		<div class="contact-info">
			<div class="left">
				<a href="https://www.google.com/maps?q=970+Upper+James+St+Hamilton,+ON+L9C+3A5+Suite+5&rlz=1C1PRFI_enPH857PH857&um=1&ie=UTF-8&sa=X&ved=0ahUKEwiPhuTe5K7jAhVlxIsBHVfrBVYQ_AUIECgB" target="_blank">
					<img src="public/images/common/map.jpg" alt="map" class="map">
				</a>
			</div>
			<div class="right">
				<h2>Contact Us</h2>
				<p>We will be glad to answer your questions, feel free to ask a piece of information or a quotation. We are looking forward to work with you.</p>
				<div class="info">
					<?php $this->info(["phone","tel","footer-phone"]); ?>
					<?php $this->info(["email","mailto","footer-email"]); ?>
					<a href="https://www.google.com/maps?q=970+Upper+James+St+Hamilton,+ON+L9C+3A5+Suite+5&rlz=1C1PRFI_enPH857PH857&um=1&ie=UTF-8&sa=X&ved=0ahUKEwiPhuTe5K7jAhVlxIsBHVfrBVYQ_AUIECgB" target="_blank" class="footer-address">
						<?php $this->info("address"); ?>
					</a>
				</div>
			</div>
		</div>
		<div class="footer-bottom">
			<div class="top">
				<a href="<?php echo URL; ?>"><img class="logo" src="public/images/common/mainLogo.png" alt="<?php $this->info("company_name"); ?>" /></a>
			</div>
			<div class="bot">
				<div class="left">
					<ul>
						<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">Home</a></li>
						<li <?php $this->helpers->isActiveMenu("services"); ?>><a href="<?php echo URL ?>services#content">Services</a></li>
						<li <?php $this->helpers->isActiveMenu("portfolio"); ?>><a href="<?php echo URL ?>portfolio#content">Portfolio</a></li>
						<li <?php $this->helpers->isActiveMenu("reviews"); ?>><a href="<?php echo URL ?>reviews#content">Reviews</a></li>
						<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact#content">Contact Us</a></li>
						<li <?php $this->helpers->isActiveMenu("privacy-policy"); ?>><a href="<?php echo URL ?>privacy-policy#content">Privacy Policy</a></li>
					</ul>
				</div>
				<div class="right">
					<p class="copy">
						Copyright © <?php echo date("Y"); ?> Copyright <?php $this->info("company_name"); ?> All Rights Reserved.
						<?php if( $this->siteInfo['policy_link'] ): ?>
							<a href="<?php $this->info("policy_link"); ?>">Terms of Use</a>.
						<?php endif ?>
					</p>
					<p class="silver"><img src="public/images/scnt.png" alt="" class="company-logo" /><a href="https://silverconnectwebdesign.com/website-development" rel="external" target="_blank">Web Design</a> Done by <a href="https://silverconnectwebdesign.com" rel="external" target="_blank">Silver Connect Web Design</a></p>
				</div>
			</div>
		</div>
	</div>
	<a class="cta" href="tel:<?php $this->info("phone") ;?>"><span class="ctc-hide">Call To Action Button</span></a>
</footer>
<textarea id="g-recaptcha-response" class="destroy-on-load"></textarea>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php echo URL; ?>public/scripts/sendform.js" data-view="<?php echo $view; ?>" id="sendform"></script>
<!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>  -->
<script src="<?php echo URL; ?>public/scripts/responsive-menu.js"></script>
<script src="https://unpkg.com/sweetalert2@7.20.10/dist/sweetalert2.all.js"></script>

<?php if( $this->siteInfo['cookie'] ): ?>
	<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
	<script src="<?php echo URL; ?>public/scripts/cookie-script.js"></script>
	<script>
	window.addEventListener("load", function(){
	window.cookieconsent.initialise({
	  "palette": {
	    "popup": {
	      "background": "#000"
	    },
	    "button": {
	      "background": "#3085d6"
	    }
	  }
	})});
	</script>
<?php endif ?>

<?php if(in_array($view,["home","contact"])): ?>
	<script src='//www.google.com/recaptcha/api.js?onload=captchaCallBack&render=explicit' async defer></script>
	<script>
		var captchaCallBack = function() {
			$('.g-recaptcha').each(function(index, el) {
				var recaptcha = grecaptcha.render(el, {'sitekey' : '<?php $this->info("site_key");?>'});
				$( '.destroy-on-load' ).remove();
			})
		};

		$('.consentBox').click(function () {
		    if ($(this).is(':checked')) {
		    	if($('.termsBox').length){
		    		if($('.termsBox').is(':checked')){
		        		$('.ctcBtn').removeAttr('disabled');
		        	}
		    	}else{
		        	$('.ctcBtn').removeAttr('disabled');
		    	}
		    } else {
		        $('.ctcBtn').attr('disabled', true);
		    }
		});

		$('.termsBox').click(function () {
		    if ($(this).is(':checked')) {
	    		if($('.consentBox').is(':checked')){
	        		$('.ctcBtn').removeAttr('disabled');
	        	}
		    } else {
		        $('.ctcBtn').attr('disabled', true);
		    }
		});

	</script>

<?php endif; ?>


<?php if ($view == "gallery"): ?>
	<script type="text/javascript" src="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.js?v=2.1.5"></script>
	<script type="text/javascript" src="<?php echo URL; ?>public/scripts/jquery.pajinate.js"></script>
	<script>
		$('#gall1').pajinate({ num_page_links_to_display : 3, items_per_page : 10 });
		$('.fancy').fancybox({
			helpers: {
				title : {
					type : 'over'
				}
			}
		});
	</script>
<?php endif; ?>
<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
<script>
  WebFont.load({
    google: {
      families: ['Great Vibes', 'Open+Sans:400,400i', 'Playfair Display:700']
    }
  });
</script>

<?php $this->checkSuspensionFooter(); ?>
</body>
</html>
