<div id="content1">
	<div class="row">
		<div class="left">
			<div class="text">
				<h2>Create Your Style with Us.</h2>
				<p>Garden Nails & Spa is dedicated to providing you with affordable, highly professional nail and body care with a modern, warm, inviting, hygienic spa atmosphere where ultra-luxurious personalized treatment abounds.</p>
				<a href="services#content" class="btn">Learn More</a>
			</div>
			<div class="image">
				<img src="public/images/common/towel-flower.png" alt="towel-flower" class="towel-flower">
			</div>
		</div>
		<div class="right">
			<div class="container">
				<div class="service">
					<img src="public/images/content/service1.jpg" alt="Manicure & Pedicure">
					<p>Manicure & Pedicure</p>
				</div>
			</div>
			<div class="container">
				<div class="service">
					<img src="public/images/content/service2.jpg" alt="Eyelash Extension">
					<p>Eyelash Extension</p>
				</div>
				<div class="service">
					<img src="public/images/content/service3.jpg" alt="Waxing Services">
					<p>Waxing Services</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="welcome">
	<div class="row">
		<h3>Welcome</h3>
		<h1>Garden Nails & Spa</h1>
		<p class="desc">Come to Garden Nails and Spa is your are in need of a nail spa in the Hamilton, ON area. Here at Garden Nails and Spa, we offer a haven of impeccable service in stylish surroundings. Come to Garden Nails and Spa and experience sheer bliss as you relax, restore and revitalize.</p>
		<div class="container">
			<div class="perk">
				<img src="public/images/content/perk1.png" alt="Perk 1">
				<p>Open 7 days a week</p>
			</div>
			<div class="perk middle">
				<img src="public/images/content/perk2.png" alt="Perk 1">
				<p>Walk-ins welcome</p>
			</div>
			<div class="perk">
				<img src="public/images/content/perk3.png" alt="Perk 1">
				<p>Gift certificates available</p>
			</div>
		</div>
	</div>
</div>
<div id="reviews">
	<div class="row">
		<div class="first">
			<div class="top">
				<img src="public/images/content/rvw1.jpg" alt="review 1">
			</div>
			<div class="bot">
				<img src="public/images/content/rvw2.jpg" alt="review 2">
				<img src="public/images/content/rvw3.jpg" alt="review 3">
			</div>
		</div>
		<div class="second">
			<img src="public/images/content/quote.png" alt="quote">
			<p class="comment">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
			<p class="author">
				— Jane Doe
			</p>
			<a href="reviews#content" class="btn">Read More</a>
		</div>
		<div class="third">
			<div class="top">
				<img src="public/images/content/rvw4.jpg" alt="review 4">
			</div>
			<div class="bot">
				<img src="public/images/content/rvw5.jpg" alt="review 5">
			</div>
		</div>
	</div>
</div>
<div id="services">
	<div class="row">
		<h3>What We Do</h3>
		<h2>Our Services</h2>
		<p>We cater the thing you need</p>
		<div class="container">
			<div class="services">
				<a href="services#content">
					<img src="public/images/content/svc1.jpg" alt="Services 1">
					<p>Manicure</p>
				</a>
			</div>
			<div class="services">
				<a href="services#content">
					<img src="public/images/content/svc2.jpg" alt="Services 2">
					<p>Pedicure</p>
				</a>
			</div>
			<div class="services">
				<a href="services#content">
					<img src="public/images/content/svc3.jpg" alt="Services 3">
					<p>Eyelash Extension</p>
				</a>
			</div>
			<div class="services">
				<a href="services#content">
					<img src="public/images/content/svc4.jpg" alt="Services 4">
					<p>Bio Gel With French</p>
				</a>
			</div>
			<div class="services">
				<a href="services#content">
					<img src="public/images/content/svc5.jpg" alt="Services 5">
					<p>Waxing</p>
				</a>
			</div>
			<div class="services">
				<a href="services#content">
					<img src="public/images/content/svc6.jpg" alt="Services 6">
					<p>Nail Art Design</p>
				</a>
			</div>
		</div>
		<a href="services#content" class="btn">Price List</a>
	</div>
</div>
<div id="title">
	<div class="row">
		<h2>A Haven Of Impeccable Service</h2>
		<h3>in stylish surroundings</h3>
		<p>Here you will experience sheer bliss as you relax, restore and revitalize. We do it for yourself and for those you love.</p>
		<a href="contact#content" class="btn">Contact Us</a>
	</div>
</div>
